package com.barelan.books;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
public class BooksApplication {

	public static void main(String[] args) {
		SpringApplication.run(BooksApplication.class, args);
	}

}

@Configuration
class RestTemplateConfig{

	@Bean
	RestTemplate restTemplate(){
		return new RestTemplate();
	}
}

@Service
class BookService {
	private final RestTemplate restTemplate;

	public BookService(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}

	public String getBookDetails(String isbn) throws JsonProcessingException {
		String apiUrl = "https://openlibrary.org/api/books?bibkeys=ISBN:" + isbn + "&format=json";
		String response = restTemplate.getForObject(apiUrl, String.class);
		ObjectMapper objectMapper = new ObjectMapper();
		Object json = objectMapper.readValue(response, Object.class);
		return objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(json);
	}
}

@RestController
class BookController {
	private final BookService bookService;

	public BookController(BookService bookService) {
		this.bookService = bookService;
	}

	@GetMapping("/books/{bookISBN}")
	public String getBook(@PathVariable String bookISBN) throws JsonProcessingException {
		return bookService.getBookDetails(bookISBN);
	}
}